function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
};

// function HEXtoRGB(h) {
//     let r = parseInt(h.slice(1, 3), 16);
//     let g = parseInt(h.slice(3, 5), 16);
//     let b = parseInt(h.slice(5, 7), 16);
//     return {r, g, b};
// };

function numberToHEX(n) {
    let h = n.toString(16);
    return h.length === 1 ? '0' + h : h;
};

function RGBtoHEX(r, g, b) {
    return '#' + numberToHEX(r) + numberToHEX(g) + numberToHEX(b);
};

function toRGBObject(rgb) {
    const [r, g, b] = rgb.match(/\d+/g).map(Number);
    return {r, g, b, dominent: false};
};

let getColourArray = function() {
    const cells = document.querySelectorAll("#mainGame td");
    let colourArr = [];
    for(let i = 0; i < cells.length; i ++) {
        let bc = cells[i].style.backgroundColor;
        colourArr.push(toRGBObject(bc));
    }
    return colourArr;
};

