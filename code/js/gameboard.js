'use strict';

function checkValidity() {
    const input = document.querySelectorAll("input");
    for(let i = 0; i < input.length; ++ i) {
        if(!input[i].checkValidity()){
            return false;
        };
    };
    return true;
};

function generateTable(initButton) {
    const size = document.getElementById("size").value;
    const table = document.querySelector("table");
    table.textContent = undefined;
    const new_tbody = document.createElement("tbody");
    table.appendChild(new_tbody);
    const tbody = document.getElementsByTagName("tbody")[0];
    
    for(let i = 0; i < size; i ++) {

        let tr = document.createElement("tr");
        tbody.appendChild(tr);
        let thisRow = document.getElementsByTagName("tr")[i];

        for(let j = 0; j < size; j ++) {

            let td = document.createElement("td");
            thisRow.appendChild(td);
            let thisCell = thisRow.childNodes[j];

            thisCell.style.maxWidth = 1/size;
            thisCell.style.maxHeight = 1/size;

            if(initButton.disabled){
                thisCell.style.backgroundColor = generateColour();
            }else {
                thisCell.style.border = "1px solid black";
            };
        };
    };
};

function generateColour() {
    const diff = document.getElementById("diff").value;
    let range = 0;
    switch(diff) {
        case '0':
            range = 128;
            break;
        case '1':
            range = 40;
            break;
        case '2':
            range = 20;
            break;
        case '3':
            range = 5
    };

    let r = getRandomInt(0, 256);
    let g = getRandomInt(r - range, r + range);
    let b = getRandomInt(r - range, r + range);

    if(g < 0) {
        g = 0;
    }else if(g > 255) {
        g = 255;
    };
    if(b < 0) {
        b = 0;
    }else if(b > 255) {
        b = 255;
    };
    
    return RGBtoHEX(r, g, b);
};

function selectBox(e) {
    if(e.target.tagName == 'TD'){
        e.target.classList.toggle("selected");
    }else {
        e.target.parentElement.classList.toggle("selected");
    };
};

function getTargetNum(arr, colour) {
    let count = 0;
    if(colour == 'red') {
        arr.forEach((element) => {
            if(element.r > element.g && element.r > element.b) {
                count ++;
            };
        });
    }else if(colour == 'green') {
        arr.forEach((element) => {
            if(element.g > element.r && element.g > element.b) {
                count ++;
            };
        });
    }else {
        arr.forEach((element) => {
            if(element.b > element.g && element.b > element.r) {
                count ++;
            };
        });
    }
    return count;
};

function getSelectedNum() {
    const tableCells = document.querySelectorAll("#mainGame td");
    let count = 0;
    for(let i = 0; i < tableCells.length; i ++) {
        if(tableCells[i].classList.contains("selected")) {
            count ++;
        };
    };
    return count;
};

function displayRGB() {
    const tableCells = document.querySelectorAll('#mainGame td');
    for(let i = 0; i < tableCells.length; i ++) {
        tableCells[i].textContent = tableCells[i].style.backgroundColor + getColour(tableCells[i].style.backgroundColor);
    };
};

function getColour(rgb) {
    const [r, g, b] = rgb.match(/\d+/g).map(Number);
    if(r > g && r > b) {
        return 'red';
    }else if(g > r && g > b) {
        return 'green';
    }else {
        return 'blue';
    };
};

function getCorrectNum(arr, colour) {
    let count = 0;
    const tableCells = document.querySelectorAll('#mainGame td');
    if(colour == 'red') {
        for(let i = 0; i < tableCells.length; i ++) {
            if(arr[i].r > arr[i].g && arr[i].r > arr[i].b && tableCells[i].classList.contains("selected")) {
                count ++;
            };
        };
    }else if(colour == 'green') {
        for(let i = 0; i < tableCells.length; i ++) {
            if(arr[i].g > arr[i].r && arr[i].g > arr[i].b && tableCells[i].classList.contains("selected")) {
                count ++;
            };
        };
    }else {
        for(let i = 0; i < tableCells.length; i ++) {
            if(arr[i].b > arr[i].g && arr[i].b > arr[i].r && tableCells[i].classList.contains("selected")) {
                count ++;
            };
        };
    };
    return count;
};

function resetBoard() {
    const table = document.querySelector("table");
    table.textContent = undefined;
    const nameInput = document.getElementById("name");
    nameInput.value = '';
    const colourInput = document.getElementById("colour");
    colourInput.value = '#ffffff';
    const initButton = document.getElementById("initialize");
    initButton.style.backgroundColor = 'rgb(211, 211, 211)';
}



