//TODO: Use functional array methods: you must use at least one of Array.foreach,
//Array.reduce, or Array.map.
function setup(e) {
    loadScoreBoard();
    const initButton = document.getElementById("initialize");
    const submitButton = document.getElementById("submit");
    submitButton.disabled = true;
    

    initButton.style.backgroundColor = 'rgb(211, 211, 211)';
    const colourInput = document.getElementById("colour");
    colourInput.addEventListener("change", (e) => {
        initButton.style.backgroundColor = colourInput.value;
    });
    //generate template 3x3 table as default
    generateTable(initButton);

    initButton.addEventListener("click", function() {
        if(checkValidity()) {
            initButton.disabled = true;
            generateTable(initButton);
            submitButton.disabled = false;
        }
    });

    const table = document.querySelector("table");
    table.addEventListener("click", selectBox);

    document.addEventListener("keydown", function(e) {
        if(e.ctrlKey && e.key === 'c') {
            displayRGB();
        };
    });

    submitButton.addEventListener("click", (e) => {
        const userColour = getColour(initButton.style.backgroundColor);
        const colourArr = getColourArray();
        const selected = getSelectedNum();
        const target = getTargetNum(colourArr, userColour);
        const resulth1 = document.getElementById("result");
        const correctNum = getCorrectNum(colourArr, userColour);
        const size = document.getElementById("size").value;
        const boardSize = size * size;
        const diff = document.getElementById("diff").value;
        resulth1.textContent = "Seach for " + userColour + ". Num of target: " + target + ". Num of selected: " + selected + ". Num of correct: " + correctNum;
        const score = getScore(correctNum, selected, boardSize, diff)
        const nameInput = document.getElementById("name").value;
        updateLocalStorage(nameInput, score);
        loadScoreBoard();
        alert("your score is " + score);
        resetBoard();
        initButton.disabled = false;
        submitButton.disabled = true;       
    })

    const scoreBoard = document.querySelector("#highScore");
    scoreBoard.addEventListener("click", (e) => {
        if(scoreBoard.children[0].style.flexDirection == 'column-reverse') {
            scoreBoard.children[0].style.flexDirection = 'column';
        }else {
            scoreBoard.children[0].style.flexDirection = 'column-reverse';
        };
    });
};  
document.addEventListener("DOMContentLoaded", setup);