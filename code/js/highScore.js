function getScore(numCorrect, numSelected, boardSize, difficulty) {
    const percent = (2 * numCorrect - numSelected) / (boardSize * boardSize);
    return Math.floor(percent * 100 * boardSize * (difficulty + 1));
};

function loadScoreBoard() {
    const nameArr = [];
    const scoreArr = [];
    for(let i = 0; i < localStorage.length; i ++) {
        nameArr.push(localStorage.key(i));
        scoreArr.push(Number(localStorage.getItem(localStorage.key(i))));
    };
    let result = sortScore(nameArr, scoreArr);
    const rows = document.querySelectorAll("#highScore tr");
    if(localStorage.length > 0) {
        for(let i = 0; i < nameArr.length; i ++) {
            rows[i].children[0].textContent = result.nameArr[i];
            rows[i].children[1].textContent = result.scoreArr[i];
        };
    };
};

function sortScore(nameArr, scoreArr) {
    for (let i = 0; i < nameArr.length; i++) {
        let lowest = i;
        for (let j = i + 1; j < nameArr.length; j++) {
          if (scoreArr[j] < scoreArr[lowest]) {
            lowest = j;
          };
        };
        if (lowest !== i) {
          [nameArr[i], nameArr[lowest]] = [nameArr[lowest], nameArr[i]];
          [scoreArr[i], scoreArr[lowest]] = [scoreArr[lowest], scoreArr[i]];
        };
      };
    if(nameArr.length > 10) {
        nameArr.pop();
        scoreArr.pop();
    };
    return {nameArr, scoreArr};
};

function updateLocalStorage(nameInput, score) {
    localStorage.setItem(nameInput, score);
    const nameArr = [];
    const scoreArr = [];
    for(let i = 0; i < localStorage.length; i ++) {
        nameArr.push(localStorage.key(i));
        scoreArr.push(Number(localStorage.getItem(localStorage.key(i))));
    };
    let result = sortScore(nameArr, scoreArr);
    localStorage.clear();
    for(let i = 0; i < result.nameArr.length; i ++) {
        localStorage.setItem(result.nameArr[i], result.scoreArr[i]);
    };
};